extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	if Autoload.isEnd:
		$AnimationPlayer.speed_scale=5
		$AnimationPlayer.play_backwards("def")
	else:
		$AnimationPlayer.speed_scale=1
		$AnimationPlayer.play("def")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func loadLevel():
	if !Autoload.isEnd:
		get_tree().change_scene_to_file("res://levels/level1/def.tscn")
	else:
		Autoload.stopMusic()

func endGame():
	if Autoload.isEnd:
		get_tree().quit()
