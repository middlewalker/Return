# Return, a small platformer with small time-travel
CONTROLS:

  a - move left

  d - move right

  [space] - jump

  leftclick(on ground) - attack

  leftclick(in air) - dash attack

  left shift - bounce attack

  rightclick - return to your shadow

  
  https://middlewalker.itch.io/return
