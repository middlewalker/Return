extends Node
var isEnd=false
var isFullscreen=false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("fullscreen"):
		if isFullscreen:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
			isFullscreen=false
		else:
			DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
			isFullscreen=true

func play(audio : AudioStream):
	if $AudioStreamPlayer.playing==false:
		$AudioStreamPlayer.stream=audio
		$AudioStreamPlayer.playing=true

func stopMusic():
	$AudioStreamPlayer.playing=false

func loadLevel(str:String):
	get_tree().change_scene_to_file(str)
