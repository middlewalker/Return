extends CharacterBody2D

@export var spd = 800.0
@export var mxspd = 1200.0
@export var grv = 9.8 * 400
@export var jmp = 300.0
@export var shiftAmount=2
@export var atkColor : Color
@export var nrmColor : Color
@onready var sprite=$vis/Sprite2D
@onready var spritePos=$vis/Sprite2D.position
@onready var atkCol=$atkCol
@onready var atkPart=$atkCol/CPUParticles2D
@onready var jmpSnd="res://sfx/sfx/export/jump.wav"
@onready var atkSnd="res://sfx/sfx/export/attack.wav"
@onready var hmgSnd="res://sfx/sfx/export/homing.wav"
@onready var bncSnd="res://sfx/sfx/export/bounce.wav"
@onready var xpdSnd="res://sfx/sfx/export/explode.wav"
@onready var dieSnd="res://sfx/sfx/export/die.wav"
@onready var audio=$AudioStreamPlayer
var inV = Vector2.ZERO
var mouseV=Vector2.ZERO
var shiftedPos=self.position
var shiftedVel=self.velocity
var canMove=false
var jumping=false
var inJump = false
var attacking=false
var inAtk=false
var dashing=false
var dashReady=false
var knockback=false
var stomping=false
var jmpVal= jmp*10
var dshspd=3000
var dashAmt=0.1
var atkDsh=mxspd*2
var stmpAmt=dshspd
var bounceAmt=1000
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	inV.x=(Input.get_action_strength("right") - Input.get_action_strength("left"))
#	inV.y=(Input.get_action_strength("down") - Input.get_action_strength("up"))
	inV=inV.normalized()

func _physics_process(delta):
	$atkCol.look_at(get_global_mouse_position())
#		jumping=false
	if Input.is_action_just_pressed("jump") && is_on_floor():
		sfx(jmpSnd)
		jmpVal=jmp/2
		velocity.y -= jmp
		jumping=true
		inJump=true
		attacking=true
	if jumping:
		if Input.is_action_pressed("jump"):
			if velocity.y < 0:
				jmpVal -= jmp*2*delta
				jmpVal=clamp(jmpVal,0,jmp)
				velocity.y -= jmpVal
				pass
		else:
			jumping=false

	if is_on_floor():
		dashReady=false
		if inV != Vector2.ZERO:
			velocity.x+=inV.x*delta*spd
		else:
			velocity.x=lerp(velocity.x,0.0,delta*2)
	else:
		if Input.is_action_just_pressed("stomp"):
			stomp()
		velocity.x += inV.x*delta*spd 
	
	velocity.y += grv * delta
	velocity.y=clamp(velocity.y,velocity.y,stmpAmt)
	if !Input.is_action_pressed("backtrack"):
		move_and_slide()
		sprite.position=spritePos-velocity*delta*2
	

	if Input.is_action_just_released("airDash") && dashReady:
		dashReady=false
		dashing=true
		dash()
	if is_on_floor():
		if dashing:
			dashing=false
			attacking=false
		if inJump:
			inJump=false
			jumping=false
			attacking=false
		if velocity.x > mxspd:
			velocity.x=lerp(velocity.x,mxspd,delta*2)
		if velocity.x < -mxspd:
			velocity.x=lerp(velocity.x,-mxspd,delta*2)
		if Input.is_action_just_pressed("atk") && !inAtk:
			atk()
	elif Input.is_action_pressed("airDash") && (!dashing || dashReady):
		dashReady=true
		if !knockback:
			dampen(5)
		attacking=true
	if dashReady && is_on_floor():
			dashReady=false
			attacking=false
	timeShift(position,velocity)
	$shadow.global_position=shiftedPos
	
	if attacking:
		$vis/Sprite2D2.material.set_shader_parameter("clr",atkColor)
		$Area2D.monitorable=true
		$Area2D.monitoring=true
	else:
		$vis/Sprite2D2.material.set_shader_parameter("clr",nrmColor)
		$Area2D.monitorable=false
		$Area2D.monitoring=false
	if stomping:
		if is_on_floor():
			sfx(bncSnd)
			velocity.y = -bounceAmt
			stomping=false
			attacking=false
		else:
			attacking=true

func timeShift(pos,vel):
	if Input.is_action_pressed("backtrack"):
		position=shiftedPos
		velocity=shiftedVel
	await get_tree().create_timer(shiftAmount).timeout
	shiftedPos=pos
	shiftedVel=vel

func dash():
	sfx(atkSnd)
	velocity=get_local_mouse_position().normalized()*dshspd
	$vis/homing.emitting=true
	await get_tree().create_timer(dashAmt).timeout
	$vis/homing.emitting=false
	dampen(1.2)

func dampen(amt):
	velocity= velocity.clamp(-Vector2.ONE*spd/amt,Vector2.ONE*spd/amt)

func atk():
	sfx(atkSnd)
	attacking=true
	atkCol.monitorable=true
	atkPart.emitting=true
	inAtk=true
	velocity.x = get_local_mouse_position().normalized().x*atkDsh
	await get_tree().create_timer(0.1).timeout
	inAtk=false
	velocity.x/=3
	await get_tree().create_timer(0.1).timeout
	if !inAtk:
		attacking=false
		atkCol.monitorable=false
		atkPart.emitting=false

func stomp():
	if !stomping:
		sfx(atkSnd)
	stomping=true
	velocity.y = stmpAmt

func _on_area_2d_area_entered(area):
	if is_on_floor():
		velocity.x /= 2
	else:
		velocity.y = -jmp*4
	if dashReady:
		knockback=true
		await get_tree().create_timer(0.2).timeout
		knockback=false
	if dashing:
		dashReady=true

func _knockback(pos,spdd):
	velocity=(global_position-pos) * spdd


func _on_area_2d_2_area_entered(area):
	var arPar=area.get_parent()
	if !attacking:
		_knockback(arPar.global_position,arPar.knckAmt)


func _on_death_col_1_area_entered(area):
	$Camera2D.position_smoothing_speed=0
	sfx(dieSnd)
	await get_tree().create_timer(1.5).timeout
	die()

func die():
	get_tree().reload_current_scene()

func sfx(string1:String):
	audio.stream=load(string1)
	audio.play()
