extends Sprite2D
var knckAmt=60
# Called when the node enters the scene tree for the first time.
func _ready():
	$range.scale=Vector2.ONE/scale
	set_process(false)
	visible=false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	look_at(get_global_mouse_position())


func _on_area_2d_area_entered(area):
	var deathPart=load("res://tech/deathPart.tscn")
	var childInst=deathPart.instantiate()
	childInst.position=self.position
	get_parent().add_child(childInst)
	queue_free()


func _on_range_area_entered(area):
	visible=true
	set_process(true)


func _on_range_area_exited(area):
	visible=false
	set_process(false)
